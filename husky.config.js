const tasks = arr => arr.join(" && ");

module.exports = {
  hooks: {
    "pre-commit": "npx jest",
    "commit-msg": "commitlint -E HUSKY_GIT_PARAMS",
    "pre-push": "git branch | grep \"*\" | egrep -v \"^* master$\""
  }
};