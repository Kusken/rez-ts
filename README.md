# Rez TypeScript

So you think error-handling in TypeScript sucks? Congratulations you've found the right lib!

**Rez** provides an alternative way to handle failures. A way that is:

* __Transparent:__ Makes all possible failures visible via __function signatures__.
* __Rigorous:__ Makes it possible to define and __handle errors by type__.
* __Enforcing__: Makes __error-handling__ a must.
* __Fluent__: Makes advanced error-handling __easy to reason about__.

## Installation
Run `npm install rez-ts`.
Then `import { define, AsyncResult, Result, exhaust } from "rez-ts";`


## Usage

Rez allows you to define a fluent result monad that can hold a rigorous set of typed failures that all become transparent within the type system:


```ts
public trySaveToDB = <TName extends DBStoreName>(plainRecord: DTO<TName>): AsyncResult<"()", "db: no connection" | "db: non-existing store">;
```

The first type argument of the `AsyncResult`, I.e `"()"` represents the value of the result if the function should succeed. The second type `"db: no connection" | "db: non-existing store"` represents known failure codes the function can fail with.

Using pattern matching we can easily handle each failure separately with side effects.

```ts
await trySaveToDB(record)
    .onSuccess(notifyRecordSaved)
    .onFailure(failure => {
        if (failure.type === "db: no connection")
            handleNoConnection(failure.error);
        else if (failure.type === "db: non-existing store")
            handleNoStore(failure.error);
        else
            exhaust(failure.type);
    });
```

This way of handling failures is not always ideal. Sometimes we need to design with fault tolerance in mind, __we need a way to recover__.

```ts
 await trySaveToDB(record)
   .tryRecoverFrom("db: no connection")
   .with(putInQueueStorage); 
 ```
After a recovery we're free to continue traveling on our happy path result chain.