const changelog = [
    "@semantic-release/changelog",
    {
        changelogFile: "CHANGELOG.md"
    }
];

const git = [
    "@semantic-release/git",
    {
        assets: [
            [
                "CHANGELOG.md",
                "package.json"
            ]
        ],
        message: "Release <%= nextRelease.version %> - <%= new Date().toLocaleDateString('en-US', {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' }) %> [skip ci]\n\n<%= nextRelease.notes %>"
    }
];

const gitlab = [
    "@semantic-release/gitlab",
    {
        gitlabUrl: "https://gitlab.com",
        gitlabApiPathPrefix: "/api/v4"
    }
];

const npm = "@semantic-release/npm";



const verifyConditions = [
    changelog,
    git,
    gitlab,

];

const generateNotes = ["@semantic-release/release-notes-generator"];



const analyzeCommits = [
    "@semantic-release/commit-analyzer"
];


const prepare = [
    changelog,
    npm,
    git
];


const publish = [
    npm,
    gitlab,
];

const addChannel = [npm];

module.exports = {
    branches: ["master"],
    verifyConditions,
    analyzeCommits,
    verifyRelease: [],
    generateNotes,
    prepare,
    publish,
    fail: [],
    success: [],
    addChannel
};