import { Result } from "./Result";
import { AsyncResult } from "./AsyncResult";
import { Success, Failure } from "./types";


const end = <TValue, TFailureCode extends string>() => ({
    succeed: makeSuccessResult<TValue, TFailureCode>(),
    fail: makeFailResult<TValue, TFailureCode>(),
    async: makeAsyncResult<TValue, TFailureCode>()
});

const makeSuccessResult = <TValue, TFCode extends string>() => (v: TValue) => Result.succeed<TValue, TFCode>(v);


type FailArgs<TFCode> = [NonNullable<TFCode>, string?] | [NonNullable<TFCode>, Error];

const makeFailResult = <TValue, TFCode extends string>() => (f: FailArgs<TFCode>) => 
    Result.fail<TValue, TFCode>(new Failure<TFCode>(f));

const makeAsyncResult = <TValue, TFCode extends string>() => (asyncFn: () => Promise<Result<TValue, TFCode>>) =>
    new AsyncResult<TValue, TFCode>(asyncFn());


export const define = {
    result: <TValue = never, TFailureCode extends NonNullable<Exclude<string, Success>> = never>() => end<TValue, TFailureCode>()
}
