import { Result } from "./Result";
import { AsyncResult } from "./AsyncResult";

export type Unit = "()";

export type Success = "success";


export type FailArgs<TFCode> = [NonNullable<TFCode>, string?] | [NonNullable<TFCode>, Error];

export class Failure<TFCode extends string> {

    public readonly type: TFCode;
    public readonly error: Error;

    constructor(args: FailArgs<TFCode>) {
        if (typeof args[1] === "string" || typeof args[1] === "undefined") {
            this.type = args[0];
            this.error = new Error(args[1]);
        }
        else {
            this.type = args[0];
            this.error = args[1];
        }
    }
}

export type Bind<TValue, TBindValue, TBindFailureCase extends string> = (v: TValue) => Result<TBindValue, TBindFailureCase>;
export type AsyncBind<TValue, TBindValue, TBindFailureCase extends string> = (v: TValue) => AsyncResult<TBindValue, TBindFailureCase>;

export type Map<TValue, TMapValue> = (v: TValue) => TMapValue;
export type AsyncMap<TValue, TMapValue> = (v: TValue) => Promise<TMapValue>;

export type Status<TValue, TFCode extends string> = 
    { id: "success"; value: TValue } | 
    { id: "failure"; failure: Failure<TFCode> };
