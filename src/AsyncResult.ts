/* eslint-disable prettier/prettier */
import { exhaust } from "./exhaust";
import { Result } from "./Result";
import { Map, Bind, AsyncBind, AsyncMap, Unit, Failure, FailArgs } from "./types";


/**
 * Async version of the Result monad.
 */
export class AsyncResult<TValue, TFCode extends string> {

    private readonly typeId: "Rez-async-result" = "Rez-async-result";

    public readonly promise: Promise<Result<TValue, TFCode>>;

    constructor(promise: Promise<Result<TValue, TFCode>>) {
        this.promise = promise;
    }

    /**
     * Lifts a type `a` into `Result<a>`.
     * 
     * @remarks
     * Also known as fmap, lift and select.
     * Takes both sync and async handlers.
     * @example
     * ```ts
     * const fn = (currentResultValue: number) => currentResultValue ? "yes" : "no";
     * 
     * const someStrResult = someNumResult.map(fn);
     * 
     * ```
     * 
     * In case `someNumResult` holds a failure the failure will be propagated to 'someStrResult'.
     */
    public map = <TMapValue>(fn: AsyncMap<TValue, TMapValue> | Map<TValue, TMapValue>): AsyncResult<TMapValue, TFCode> => {
        const resultPromise = this.promise.then(currRes => {
            if (currRes.status.id === "success") {
                const mapResult = fn(currRes.status.value);
                if (mapResult instanceof Promise) {
                    return mapResult.then(mapRes => new Result<TMapValue, TFCode>({ id: "success", value: mapRes }));
                }
                else {
                    return new Result<TMapValue, TFCode>({ id: "success", value: mapResult })
                }
            }
            else if (currRes.status.id === "failure") {
                return new Result<TMapValue, TFCode>(currRes.status);
            }
            else {
                return exhaust(currRes.status);
            }
        });
        return new AsyncResult<TMapValue, TFCode>(resultPromise);
    }


    /**
     * Convienience method for lifting '()' to 'Result<()>'.
     */
    public mapToUnit = () => this.map<Unit>(() => "()");


    public static map = <TMapValue>(fn: () => Promise<TMapValue>): AsyncResult<TMapValue, never> => {
        const p = fn().then(v => Result.succeed(v));
        return new AsyncResult<TMapValue, never>(p);
    }



    /**
     * Converts a type `Result<a>` into `Result<b>`.
     * 
     * @remarks
     * Also known as flatMap, collect and selectMany.
     * Takes both sync and async handlers.
     * 
     * @example
     * ```ts
     * const fn = (currentResultValue: number) => 
     *      currentResultValue ? result.succeed("Hello world!") : result.fail("number was zero code");
     * 
     * const someStrResult = someNumResult.bind(fn);
     * 
     * ```
     * 
     * In case `someNumResult` holds a failure the failure will be propagated to 'someStrResult'.
     */
    public bind = <TBindValue, TBindFCode extends string>(fn: Bind<TValue, TBindValue, TBindFCode> | AsyncBind<TValue, TBindValue, TBindFCode>): AsyncResult<TBindValue, TFCode | TBindFCode> => {
        const promiseResult = this.promise.then(currRes => {
            if (currRes.status.id === "success") {
                const bindResult = fn(currRes.status.value);

                if (bindResult instanceof AsyncResult) {
                    return bindResult.promise.then(bindRes => new Result<TBindValue, TFCode | TBindFCode>(bindRes.status));
                }
                else {
                    return new Result<TBindValue, TFCode | TBindFCode>(bindResult.status);
                }
            }
            else if (currRes.status.id === "failure") {
                return new Result<TBindValue, TFCode | TBindFCode>(currRes.status);
            }
            else {
                return exhaust(currRes.status);
            }
        });

        return new AsyncResult<TBindValue, TFCode | TBindFCode>(promiseResult);
    }


    /**
     * Executes side-effect exclusively when the result has succeeded.
     * 
     * 
     * @example
     * ```ts
     * const emitNotification = (message: string) => {};
     * 
     * someSuccessResult.onSuccess(message => emitNotification);
     * 
     * ```
     */
    public onSuccess = (fn: (value: TValue) => void | Promise<void>): AsyncResult<TValue, TFCode> => {

        const promiseResult = this.promise.then(res => {
            if (res.status.id === "failure") {
                return this.promise;
            }
            else if (res.status.id === "success") {
                const fn2 = fn(res.status.value);

                if (fn2 instanceof Promise) {
                    return fn2.then(() => this.promise);
                }
                else {
                    return this.promise;
                }
            }
            else {
                return exhaust(res.status);
            }
        });

        return new AsyncResult<TValue, TFCode>(promiseResult);
    }

    /**
     * Executes side-effect exclusively when the result has failed.
     * 
     * 
     * @example
     * ```ts
     * const emitNotification = (message: string) => {};
     * 
     * someFailedResult.onFailure(message => emitNotification);
     * 
     * ```
     */
    public onFailure = (fn: (failure: Failure<TFCode>) => void | Promise<void>): AsyncResult<TValue, TFCode> => {

        const promiseResult = this.promise.then(res => {
            if (res.status.id === "success") {
                return this.promise;
            }
            else if (res.status.id === "failure") {
                const fn2 = fn(res.status.failure);

                if (fn2 instanceof Promise) {
                    return fn2.then(() => this.promise);
                }
                else {
                    return this.promise;
                }
            }
            else {
                return exhaust(res.status);
            }
        });

        return new AsyncResult<TValue, TFCode>(promiseResult);

    }


    /**
     * Can recover success state from an async result that's in a fail state.
     * 
     * @remarks
     * Recovery is defined fluently in two steps. 
     * 
     * Step 1.`recoverFrom` takes a set of failure types that you want to try to recover from as args.
     * 
     * Step 2.`with` takes a map or bind function that defines how to recover.
     * 
     * @example
     * ```ts
     * public trySaveToDB = <TName extends DBStoreName>(plainRecord: DTO<TName>): AsyncResult<"()", "db: no connection" | "db: non-existing store">;
     * 
     * await trySaveToDB(record)
     *   .tryRecoverFrom("db: no connection")
     *   .with(putInQueueStorage);
     *   
     * ```
     */
    tryRecoverFrom = (...fcode: TFCode[]) => ({
        with: <TBindFType extends string>(heal: AsyncMap<void, TValue> | Map<void, TValue> | AsyncBind<void, TValue, TBindFType> | Bind<void, TValue, TBindFType>): AsyncResult<TValue, TFCode | TBindFType> => {
            const promiseResult: Promise<Result<TValue, TFCode | TBindFType>> = this.promise.then(res => {
                if (res.status.id === "success") {
                    return this.promise.then(res => new Result<TValue, TFCode | TBindFType>(res.status));
                }
                else if (res.status.id === "failure") {
                    const healResult = heal();
                    const failureType = res.status.failure.type;
                    const isMatchingFailure = fcode.some(fc => fc === failureType);
                    
                    if (healResult instanceof Promise && isMatchingFailure) {
                        return healResult.then(value => {
                            if (value instanceof AsyncResult) {
                                return value.promise.then(res => new Result<TValue, TFCode | TBindFType>(res.status));
                            }
                            else {
                                return Result.succeed<TValue, TFCode | TBindFType>(value);
                            }
                        });
                    }
                    else if (!(healResult instanceof Promise) && isMatchingFailure) {
                        if (healResult instanceof AsyncResult) {
                            return healResult.promise.then(res => new Result<TValue, TFCode | TBindFType>(res.status));
                        }
                        else if (healResult instanceof Result) {
                            return new Result<TValue, TFCode | TBindFType>(healResult.status);
                        }
                        else {
                            return Result.succeed<TValue, TFCode | TBindFType>(healResult);
                        }
                        
                    }
                    else {
                        return this.promise.then(res => new Result<TValue, TFCode | TBindFType>(res.status));
                    }
                }
                else {
                    return exhaust(res.status);
                }

            });
            return new AsyncResult<TValue, TFCode | TBindFType>(promiseResult);
        }
    })


    /**
     * Asserts that an async result value meets the condition of a predicate.
     * 
     * @remarks
     * Assertions are defined fluently in two steps. 
     * 
     * Step 1.`assert` takes a predicate function.
     * 
     * Step 2.`orFail` takes `FailArgs` tuple.
     * 
     * 
     * @example
     * ```ts
     * public tryAuthenticate = (creds: Credentials): AsyncResult<Token, "bad-credentials">;
     * 
     * await tryAuthenticate(myCreds)
     *   .assert((token) => token.customClaims["verifiedCustomer"] === true)
     *   .orFail(["authorization failure: customer not verified"]);
     *   
     * ```
     */
    public assert = <TPredicateFCode extends string>(predicate: (subject: TValue) => boolean | Promise<boolean>) => ({
        orFail: (failure: FailArgs<TPredicateFCode>): AsyncResult<TValue, TFCode | TPredicateFCode> => {
            const promiseResult = this.promise.then(res => {
                if (res.status.id === "failure") {
                    return Result.fail<TValue, TFCode | TPredicateFCode>(res.status.failure);
                }
                else if (res.status.id === "success") {
                    const resVal = res.status.value;
                    const predicateIsMet = predicate(resVal);
                    if (predicateIsMet instanceof Promise) {
                        return predicateIsMet.then(v => v 
                            ? Result.succeed<TValue, TFCode | TPredicateFCode>(resVal)
                            : Result.fail<TValue, TFCode | TPredicateFCode>(new Failure<TPredicateFCode>(failure)));
                    }
                    else {
                        return predicateIsMet 
                            ? Result.succeed<TValue, TFCode | TPredicateFCode>(resVal)
                            : Result.fail<TValue, TFCode | TPredicateFCode>(new Failure<TPredicateFCode>(failure));
                    }
                }
                else {
                    return exhaust(res.status);
                }
            });

            return new AsyncResult<TValue, TFCode | TPredicateFCode>(promiseResult);
            
        }
    })

}
