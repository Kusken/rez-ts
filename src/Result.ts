/* eslint-disable prettier/prettier */
import { exhaust } from "./exhaust";
import { AsyncResult } from "./AsyncResult";
import { Status, Bind, Map, Failure, Unit, FailArgs } from "./types";


/**
 * Sync version of the Result monad.
 */
export class Result<TValue, TFCode extends string> {

    public readonly status: Status<TValue, TFCode>;

    public constructor(status: Status<TValue, TFCode>) {
        this.status = status;
    }

    public static succeed<TValue, TFCode extends string = never>(v: TValue) {
        return new Result<TValue, TFCode>({ id: "success", value: v });
    }

    public static fail<TValue, TFCode extends string>(failure: Failure<TFCode>) {

        return new Result<TValue, TFCode>({ id: "failure", failure: failure });
    }


    public onSuccess = (fn: (success: TValue) => void) => {

        if (this.status.id === "failure") {
            return new Result<TValue, TFCode>({ id: "failure", failure: this.status.failure });
        }
        else if (this.status.id === "success") {
            fn(this.status.value);
            return new Result<TValue, TFCode>({ id: "success", value: this.status.value });
        }
        else {
            return exhaust(this.status);
        }
    }

    public onFailure = (fn: (failure: Failure<TFCode>) => void) => {

        if (this.status.id === "success") {
            return new Result<TValue, TFCode>({ id: "success", value: this.status.value });
        }
        else if (this.status.id === "failure") {
            fn(this.status.failure);
            return new Result<TValue, TFCode>({ id: "failure", failure: this.status.failure });
        }
        else {
            return exhaust(this.status);
        }
    };


    /**
     * Lifts a type `a` into `Result<a>`.
     * 
     * @remarks
     * Also known as fmap, lift and select.
     * Takes sync handlers exclusively.
     * @example
     * ```ts
     * const fn = (currentResultValue: number) => currentResultValue ? "yes" : "no";
     * 
     * const someStrResult = someNumResult.map(fn);
     * 
     * ```
     * 
     * In case `someNumResult` holds a failure the failure will be propagated to 'someStrResult'.
     */
    public map = <TMapValue>(fn: Map<TValue, TMapValue>) => {
        if (this.status.id === "success") {
            return new Result<TMapValue, TFCode>({ id: "success", value: fn(this.status.value) });
        } else if (this.status.id === "failure") {
            return new Result<TMapValue, TFCode>(this.status);
        } else {
            return exhaust(this.status);
        }
    };

    /**
     * Convienience method for lifting '()' to 'Result<()>'.
     */
    public mapToUnit = () => this.map<Unit>(() => "()");


    /**
     * Converts a type `Result<a>` into `Result<b>`.
     * 
     * @remarks
     * Also known as flatMap, collect and selectMany.
     * Takes sync handlers exclusively.
     * 
     * @example
     * ```ts
     * const fn = (currentResultValue: number) => 
     *      currentResultValue ? result.succeed("Hello world!") : result.fail("number was zero code");
     * 
     * const someStrResult = someNumResult.bind(fn);
     * 
     * ```
     * 
     * In case `someNumResult` holds a failure the failure will be propagated to 'someStrResult'.
     */
    public bind = <TBindValue, TBindFailureCode extends string>(fn: Bind<TValue, TBindValue, TBindFailureCode>): Result<TBindValue, TFCode | TBindFailureCode> => {
        if (this.status.id === "success") {
            const bindResult = fn(this.status.value);
            return new Result<TBindValue, TFCode | TBindFailureCode>(bindResult.status);
        } else if (this.status.id === "failure") {
            return new Result<TBindValue, TFCode | TBindFailureCode>(this.status);
        } else {
            return exhaust(this.status);
        }
    };

    public async = (): AsyncResult<TValue, TFCode> => new AsyncResult(Promise.resolve(this));


    /**
     * Tries to recover success state from a result that's in a fail state.
     * 
     * @remarks
     * Recovery is defined fluently in two steps. 
     * 
     * Step 1.`recoverFrom` takes a set of failure types that you want to try to recover from as args.
     *
     *  Step 2.`with` takes a map or bind function that defines how to recover.
     * 
     * @example
     * ```ts
     * public trySaveToDB = <TName extends DBStoreName>(plainRecord: DTO<TName>): Result<"()", "db: no connection" | "db: non-existing store">;
     * 
     * trySaveToDB(record)
     *   .tryRecoverFrom("db: no connection")
     *   .with(putInQueueStorage);
     *   
     * ```
     */
    public tryRecoverFrom = (...fcodes: TFCode[]) => ({
        with: <TBindFCode extends string>(fn: Map<void, TValue> | Bind<void, TValue, TBindFCode>) => {
            if (this.status.id === "success") {
                return Result.succeed<TValue, TFCode | TBindFCode>(this.status.value);
            }
            else if (this.status.id === "failure") {
                const type = this.status.failure.type;
                if (fcodes.some(fc => fc === type)) {
                    const res = fn();

                    if (res instanceof Result) {
                        return new Result<TValue, TBindFCode | TFCode>(res.status);
                    }
                    else {
                        return Result.succeed<TValue, TFCode | TBindFCode>(res);
                    }

                }
                else {
                    return Result.fail<TValue, TFCode | TBindFCode>(this.status.failure);
                }
            }
            else {
                return exhaust(this.status);
            }
        }
    })


    /**
     * Asserts that a result value meets the condition of a predicate.
     * 
     * @remarks
     * Assertions are defined fluently in two steps. 
     * 
     * Step 1.`assert` takes a predicate function.
     * 
     * Step 2.`orFail` takes `FailArgs` tuple.
     * 
     * 
     * @example
     * ```ts
     * public tryAuthenticate = (creds: Credentials): Result<Token, "bad-credentials">;
     * 
     * tryAuthenticate(myCreds)
     *   .assert((token) => token.customClaims["verifiedCustomer"] === true)
     *   .orFail(["authorization failure: customer not verified"]);
     *   
     * ```
     */
    public assert = <TPredicateFCode extends string>(predicateIsMet: (subject: TValue) => boolean) => ({
        orFail: (failure: FailArgs<TPredicateFCode>) => {
        
            if (this.status.id === "failure") {
                return Result.fail<TValue, TFCode | TPredicateFCode>(this.status.failure);
            }
            else if (predicateIsMet(this.status.value)) {
                return Result.succeed<TValue, TFCode | TPredicateFCode>(this.status.value);
            }
            else {
                return Result.fail<TValue, TFCode | TPredicateFCode>(new Failure(failure));
            }
        }
    })

}