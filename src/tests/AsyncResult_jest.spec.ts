import { define } from "../define";
import { expectResult } from "./assertions";
import { Failure } from "../types";

describe("AsyncResult", () => {
    type FailureCode = "err-1" | "err-2" | "err-3";
    const failureCodes: FailureCode[] = ["err-1", "err-2", "err-3"];
    const values = [0, 12, -33];

    const sut = define.result<number, FailureCode>();

    describe("Maps failure", () => {
        test.each(failureCodes)("[%#]When result is failure async", async code => {
            const result = await sut
                .fail([code])
                .async()
                .map(() => Promise.resolve("hello")).promise;

            expectResult(result).toHaveFailureCode(code);
        });


        test.each(failureCodes)("[%#]When result is failure", async code => {
            const result = await sut
                .fail([code])
                .async()
                .map(v => v.toString()).promise;

            expectResult(result).toHaveFailureCode(code);
        });

    });

    describe("Maps success", () => {
       
        test.each(values)("[%#]When result is success async", async value => {
            const result = await sut
                .succeed(value)
                .async()
                .map(v => Promise.resolve(v.toString())).promise;

            expectResult(result).toHaveValue(value.toString());
        });


        test.each(values)("[%#]When result is success", async value => {
            const result = await sut
                .succeed(value)
                .async()
                .map(v => v.toString()).promise;

            expectResult(result).toHaveValue(value.toString());
        });
    });

    describe("Maps to unit", () => {

        test.each(values)("[%#]When result is success", async value => {
            const result = await sut
                .succeed(value)
                .async()
                .mapToUnit().promise;

            expectResult(result).toHaveValue("()");
        });
    });

    describe("Maps failure instead of unit", () => {
        test.each(failureCodes)("[%#]When result is failure", async fcode => {
            const result = await sut
                .fail([fcode])
                .async()
                .mapToUnit().promise;

            expectResult(result).toHaveFailureCode(fcode);
        });
        
    });


    describe("Binds failure", () => {
        type BindFailures = "error-33" | "error-55" | "error-66";
        const bindFailureCodes: BindFailures[] = ["error-33", "error-55", "error-66"];
        const bindResultBuilder = define.result<string, BindFailures>();

        test.each(failureCodes)("[%#]When initial result is failure and bind result is success async", async code => {
            const result = await sut
                .fail([code])
                .async()
                .bind(v => bindResultBuilder.async(async () => bindResultBuilder.succeed(v.toString()))).promise;

            expectResult(result).toHaveFailureCode(code);
        });

        test.each(failureCodes)("[%#]When both results are failures async", async code => {
            const result = await sut
                .fail([code])
                .async()
                .bind(v => bindResultBuilder.async(async () => bindResultBuilder.fail(["error-33"]))).promise;

            expectResult(result).toHaveFailureCode(code);
        });

        test.each(bindFailureCodes)("[%#]When initial result is success and bind result is failure async", async code => {
            const result = await sut
                .succeed(33)
                .async()
                .bind(v => bindResultBuilder.async(async () => bindResultBuilder.fail([code]))).promise;

            expectResult(result).toHaveFailureCode(code);
        });

        

        test.each(failureCodes)("[%#]When initial result is failure and bind result is success", async code => {
            const result = await sut
                .fail([code])
                .async()
                .bind(v => bindResultBuilder.succeed(v.toString())).promise;

            expectResult(result).toHaveFailureCode(code);
        });

        test.each(failureCodes)("[%#]When both results are failures", async code => {
            const result = await sut
                .fail([code])
                .async()
                .bind(v => bindResultBuilder.fail(["error-33"])).promise;

            expectResult(result).toHaveFailureCode(code);
        });

        test.each(bindFailureCodes)("[%#]When initial result is success and bind result is failure async", async code => {
            const result = await sut
                .succeed(33)
                .async()
                .bind(v => bindResultBuilder.fail([code])).promise;

            expectResult(result).toHaveFailureCode(code);
        });
    });


    describe("Binds success", () => {
        const bindResultBuilder = define.result<string, "failure-1">();

        
        test.each(values)("[%#]When both results are successfull async", async value => {
            const result = await sut
                .succeed(value)
                .async()
                .bind(v => bindResultBuilder.async(async () => bindResultBuilder.succeed(v.toString()))).promise;

            expectResult(result).toHaveValue(value.toString());
        });


        test.each(values)("[%#]When both results are successfull", async value => {
            const result = await sut
                .succeed(value)
                .async()
                .bind(v => bindResultBuilder.succeed(v.toString())).promise;

            expectResult(result).toHaveValue(value.toString());
        });
    });


    describe("Causes side effect on success", () => {

        test.each(values)("[%#]When result is success async", async (value) => {

            const asyncHandler = jest.fn(async (v: number) => { });

            await sut
                .succeed(value)
                .async()
                .onSuccess(asyncHandler).promise;


            expect(asyncHandler).toBeCalledTimes(1);
            expect(asyncHandler).toBeCalledWith(value);

        });

        test.each(values)("[%#]When result is success", async (value) => {

            const handler = jest.fn((v: number) => { });

            await sut
                .succeed(value)
                .async()
                .onSuccess(handler).promise;


            expect(handler).toBeCalledTimes(1);
            expect(handler).toBeCalledWith(value);

        });
    });

    describe("Causes side effect on failure", () => {
        test.each(failureCodes)("[%#]When result is failure async", async (fcode) => {

            const asyncHandler = jest.fn(async (v: Failure<FailureCode>) => { });

            await sut
                .fail([fcode])
                .async()
                .onFailure(asyncHandler).promise;

            expect(asyncHandler).toBeCalledTimes(1);
            expect(asyncHandler).toBeCalledWith(new Failure<FailureCode>([fcode, new Error()]));
        });

        

        test.each(failureCodes)("[%#]When result is failure", async (fcode) => {

            const handler = jest.fn((v: Failure<FailureCode>) => { });

            await sut
                .fail([fcode])
                .async()
                .onFailure(handler).promise;

            expect(handler).toBeCalledTimes(1);
            expect(handler).toBeCalledWith(new Failure<FailureCode>([fcode, new Error()]));
        });
    })

    describe("Doesn't cause side effect on success", () => {
        test.each(failureCodes)("[%#]When result is failure async", async (fcode) => {

            const asyncHandler = jest.fn(async (v: number) => { });

            await sut
                .fail([fcode])
                .async()
                .onSuccess(asyncHandler).promise;


            expect(asyncHandler).toBeCalledTimes(0);

        });

        test.each(failureCodes)("[%#]When result is failure", async (fcode) => {

            const handler = jest.fn((v: number) => { });

            await sut
                .fail([fcode])
                .async()
                .onSuccess(handler).promise;


            expect(handler).toBeCalledTimes(0);

        });
    });

    describe("Doesn't cause side effect on failure", () => {
        
        test.each(values)("[%#]When result is success async", async (value) => {

            const asyncHandler = jest.fn(async (v: Failure<FailureCode>) => { });

            await sut
                .succeed(value)
                .async()
                .onFailure(asyncHandler).promise;

            expect(asyncHandler).toBeCalledTimes(0);
        });


        test.each(values)("[%#]When result is success", async (value) => {

            const handler = jest.fn((v: Failure<FailureCode>) => { });

            await sut
                .succeed(value)
                .async()
                .onFailure(handler).promise;

            expect(handler).toBeCalledTimes(0);
        });
    })


    describe("Recovers from failure", () => {
        test.each(failureCodes)("[%#]With map when result is a matching failure async", async (fcode) => {

            const expected = Math.floor((Math.random() * 100) + 1);

            const result = await sut
                .fail([fcode])
                .async()
                .tryRecoverFrom(fcode)
                .with(async () => expected).promise;

            expectResult(result).toHaveValue(expected);
        });

        test.each(failureCodes)("[%#]With map when result is a matching failure", async (fcode) => {

            const expected = Math.floor((Math.random() * 100) + 1);

            const result = await sut
                .fail([fcode])
                .async()
                .tryRecoverFrom(fcode)
                .with(() => expected).promise;

            expectResult(result).toHaveValue(expected);
        });

        
        test.each(failureCodes)("[%#]With bind when result is a matching failure async", async (fcode) => {

            const expected = Math.floor((Math.random() * 100) + 1);

            const result = await sut
                .fail([fcode])
                .async()
                .tryRecoverFrom(fcode)
                .with(() => sut.async(async () => sut.succeed(expected))).promise;

            expectResult(result).toHaveValue(expected);
        });

        test.each(failureCodes)("[%#]With bind when result is a matching failure", async (fcode) => {

            const expected = Math.floor((Math.random() * 100) + 1);

            const result = await sut
                .fail([fcode])
                .async()
                .tryRecoverFrom(fcode)
                .with(() => sut.succeed(expected)).promise;

            expectResult(result).toHaveValue(expected);
        });

        
    })

    
    describe("Doesn't recover from failure", () => {
        type NonMatchingFailureCode = "err-4";
        const sut2 = define.result<number, FailureCode | NonMatchingFailureCode>();
        
        test.each(failureCodes)("[%#]With map when result isn't a matching failure async", async (fcode) => {

            const result = await sut2
                .fail([fcode])
                .async()
                .tryRecoverFrom("err-4")
                .with(async () => Math.floor((Math.random() * 100) + 1)).promise;

            expectResult(result).toHaveFailureCode(fcode);
        });

        test.each(failureCodes)("[%#]With map when result isn't a matching failure", async (fcode) => {

            const result = await sut2
                .fail([fcode])
                .async()
                .tryRecoverFrom("err-4")
                .with(() => Math.floor((Math.random() * 100) + 1)).promise;

            expectResult(result).toHaveFailureCode(fcode);
        });


        
        test.each(failureCodes)("[%#]With bind when result isn't a matching failure async", async (fcode) => {

            const result = await sut2
                .fail([fcode])
                .async()
                .tryRecoverFrom("err-4")
                .with(() => sut.async(async () => sut.succeed(Math.floor((Math.random() * 100) + 1)))).promise;

                expectResult(result).toHaveFailureCode(fcode);
        });

        test.each(failureCodes)("[%#]With bind when result isn't a matching failure", async (fcode) => {

            const result = await sut2
                .fail([fcode])
                .async()
                .tryRecoverFrom("err-4")
                .with(() => sut.succeed(Math.floor((Math.random() * 100) + 1))).promise;

                expectResult(result).toHaveFailureCode(fcode);
        });
    })


    describe("Asserts predicate", () => {
        
        type PredicateFailure = "subject did not meet predicate";
        const sut2 = define.result<number, FailureCode | PredicateFailure>();


        test.each(values)("[%#]When result is success async", async (value) => {

            const result = await sut2
                .succeed(value)
                .async()
                .assert(() => Promise.resolve(true)).orFail(["subject did not meet predicate"])
                .promise;

            expectResult(result).toHaveSuccessStatus();
                
        });

        test.each(values)("[%#]When result is success", async (value) => {

            const result = await sut2
                .succeed(value)
                .async()
                .assert(() => true).orFail(["subject did not meet predicate"])
                .promise;

            expectResult(result).toHaveSuccessStatus();
                
        });
        
        test.each(values)("[%#]And fails when result is success but predicate not met async", async (value) => {
            const result = await sut2
                .succeed(value)
                .async()
                .assert(() => Promise.resolve(false)).orFail(["subject did not meet predicate"])
                .promise;

            expectResult(result).toHaveFailureCode("subject did not meet predicate");
        });

        test.each(values)("[%#]And fails when result is success but predicate not met", async (value) => {
            const result = await sut2
                .succeed(value)
                .async()
                .assert(() => false).orFail(["subject did not meet predicate"])
                .promise;

            expectResult(result).toHaveFailureCode("subject did not meet predicate");
        });
    })
    

    describe("Doesn't assert predicate", () => {
        
        test.each(failureCodes)("[%#]When result is failure async", async (fcode) => {

            const asyncPredicate = jest.fn(async (v: number) => true);
            
            const result = await sut
                .fail([fcode])
                .async()
                .assert(asyncPredicate).orFail(["subject did not meet predicate"])
                .promise;

            expect(asyncPredicate).toHaveBeenCalledTimes(0);
            expectResult(result).toHaveFailureCode(fcode);
                
        });

        test.each(failureCodes)("[%#]When result is failure", async (fcode) => {

            const predicate = jest.fn((v: number) => true);
            
            const result = await sut
                .fail([fcode])
                .async()
                .assert(predicate).orFail(["subject did not meet predicate"])
                .promise;

            expect(predicate).toHaveBeenCalledTimes(0);
            expectResult(result).toHaveFailureCode(fcode);
                
        });
    })
});
