import { Result } from "../Result";

export const expectResult = <TValue, TFailureCode extends string>(res: Result<TValue, TFailureCode>) => ({
    toHaveValue: (v: TValue) => {
        if (res.status.id === "success") {
            expect(res.status.value).toStrictEqual(v);
        } else {
            throw new Error(`Expected result to have case.id 'success' but found ${res.status.id}`);
        }
    },

    toHaveFailureCode: (c: TFailureCode) => {
        if (res.status.id === "failure") {
            expect(res.status.failure.type).toBe(c);
        } else {
            throw new Error(`Expected result to have case.id 'failure' but found ${res.status.id}`);
        }
    },
    toHaveSuccessStatus: () => {
        if (res.status.id === "success") {
            return;
        } else {
            throw new Error(`Expected result to have case.id 'success' but found ${res.status.id}`);
        }
    }
});
