import { define } from "../define";
import { expectResult } from "./assertions";
import { Failure } from "../types";


describe("Result", () => {
    type FailureCode = "err-1" | "err-2" | "err-3";
    const failureCodes: FailureCode[] = ["err-1", "err-2", "err-3"];
    const values = [0, 12, -33];

    const sut = define.result<number, FailureCode>();

    describe("Maps failure", () => {
        test.each(failureCodes)("[%#]When result is failure", fcode => {
            const result = sut
                .fail([fcode])
                .map(() => Promise.resolve("hello"));

            expectResult(result).toHaveFailureCode(fcode);
        });

    });

    describe("Maps success", () => {

        test.each(values)("[%#]When result is success", value => {
            const result = sut
                .succeed(value)
                .map(v => v.toString());

            expectResult(result).toHaveValue(value.toString());
        });
    });

    describe("Maps to unit", () => {

        test.each(values)("[%#]When result is success", value => {
            const result = sut
                .succeed(value)
                .mapToUnit();

            expectResult(result).toHaveValue("()");
        });
    });

    describe("Maps failure instead of unit", () => {
        test.each(failureCodes)("[%#]When result is failure", fcode => {
            const result = sut
                .fail([fcode])
                .mapToUnit();

            expectResult(result).toHaveFailureCode(fcode);
        });
        
    });




    describe("Binds failure", () => {
        type BindFailures = "error-33" | "error-55" | "error-66";
        const bindFailureCodes: BindFailures[] = ["error-33", "error-55", "error-66"];
        const bindResultBuilder = define.result<string, BindFailures>();


        test.each(failureCodes)("[%#]When initial result is failure and bind result is success", code => {
            const result = sut
                .fail([code])
                .bind(v => bindResultBuilder.succeed(v.toString()));

            expectResult(result).toHaveFailureCode(code);
        });

        test.each(failureCodes)("[%#]When both results are failures", code => {
            const result = sut
                .fail([code])
                .bind(v => bindResultBuilder.fail(["error-33"]));

            expectResult(result).toHaveFailureCode(code);
        });

        test.each(bindFailureCodes)("[%#]When initial result is success and bind result is failure async", code => {
            const result = sut
                .succeed(33)
                .bind(v => bindResultBuilder.fail([code]));

            expectResult(result).toHaveFailureCode(code);
        });
    });


    describe("Binds success", () => {
        const bindResultBuilder = define.result<string, "failure-1">();


        test.each(values)("[%#]When both results are successfull", value => {
            const result = sut
                .succeed(value)
                .bind(v => bindResultBuilder.succeed(v.toString()));

            expectResult(result).toHaveValue(value.toString());
        });
    });


    describe("Causes side effect on success", () => {


        test.each(values)("[%#]When result is success", (value) => {

            const handler = jest.fn((v: number) => { });

            sut
                .succeed(value)
                .onSuccess(handler);


            expect(handler).toBeCalledTimes(1);
            expect(handler).toBeCalledWith(value);

        });
    });

    describe("Causes side effect on failure", () => {

        test.each(failureCodes)("[%#]When result is failure", (fcode) => {

            const handler = jest.fn((v: Failure<FailureCode>) => { });

            sut
                .fail([fcode])
                .onFailure(handler);

            expect(handler).toBeCalledTimes(1);
            expect(handler).toBeCalledWith(new Failure<FailureCode>([fcode, new Error()]));
        });
    })

    describe("Doesn't cause side effect on success", () => {

        test.each(failureCodes)("[%#]When result is failure", (fcode) => {

            const handler = jest.fn((v: number) => { });

            sut
                .fail([fcode])
                .onSuccess(handler);


            expect(handler).toBeCalledTimes(0);

        });
    });

    describe("Doesn't cause side effect on failure", () => {


        test.each(values)("[%#]When result is success", (value) => {

            const handler = jest.fn((v: Failure<FailureCode>) => { });

            sut
                .succeed(value)
                .onFailure(handler);

            expect(handler).toBeCalledTimes(0);
        });
    })


    describe("Recovers from failure", () => {
        test.each(failureCodes)("[%#]With map when result is a matching failure", (fcode) => {

            const expected = Math.floor((Math.random() * 100) + 1);

            const result = sut
                .fail([fcode])
                .tryRecoverFrom(fcode)
                .with(() => expected);

            expectResult(result).toHaveValue(expected);
        });

        test.each(failureCodes)("[%#]With bind when result is a matching failure", (fcode) => {

            const expected = Math.floor((Math.random() * 100) + 1);

            const result = sut
                .fail([fcode])
                .tryRecoverFrom(fcode)
                .with(() => sut.succeed(expected));

            expectResult(result).toHaveValue(expected);
        });

        
    })

    describe("Doesn't recover from failure", () => {
        type NonMatchingFailureCode = "err-4";
        const sut2 = define.result<number, FailureCode | NonMatchingFailureCode>();
        
        test.each(failureCodes)("[%#]With map when result isn't a matching failure", (fcode) => {

            const result = sut2
                .fail([fcode])
                .tryRecoverFrom("err-4")
                .with(() => Math.floor((Math.random() * 100) + 1));

            expectResult(result).toHaveFailureCode(fcode);
        })

        test.each(failureCodes)("[%#]With bind when result isn't a matching failure", (fcode) => {

            const result = sut2
                .fail([fcode])
                .tryRecoverFrom("err-4")
                .with(() => sut.succeed(Math.floor((Math.random() * 100) + 1)));

                expectResult(result).toHaveFailureCode(fcode);
        });
    })

    describe("Asserts predicate", () => {
        
        type PredicateFailure = "subject did not meet predicate";
        const sut2 = define.result<number, FailureCode | PredicateFailure>();

        test.each(values)("[%#]When result is success", (value) => {

            const result = sut2
                .succeed(value)
                .assert(() => true).orFail(["subject did not meet predicate"]);

            expectResult(result).toHaveValue(value);
                
        });

        test.each(values)("[%#]And fails when result is success but predicate not met", (value) => {
            const result = sut2
                .succeed(value)
                .assert(() => false).orFail(["subject did not meet predicate"]);

            expectResult(result).toHaveFailureCode("subject did not meet predicate");
        })
    })

    describe("Doesn't assert predicate", () => {
        
        test.each(failureCodes)("[%#]When result is failure", (fcode) => {

            const predicate = jest.fn((v: number) => true);
            
            const result = sut
                .fail([fcode])
                .assert(predicate).orFail(["subject did not meet predicate"]);

            expect(predicate).toHaveBeenCalledTimes(0);
            expectResult(result).toHaveFailureCode(fcode);
                
        });
    })
});
