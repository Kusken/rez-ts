# [1.4.0](https://gitlab.com/Kusken/rez-ts/compare/v1.3.0...v1.4.0) (2020-06-14)


### Bug Fixes

* **result.ts:** asserting failed results wrongly returns assert failure ([e94eaac](https://gitlab.com/Kusken/rez-ts/commit/e94eaacc8be6458bda869f3d488b087476e3c431))


### Features

* **asyncresult.ts:** added assert functionality for async result ([7a5dfda](https://gitlab.com/Kusken/rez-ts/commit/7a5dfdaa34ec5325d343f8417ee2ec0cc38ef612))

# [1.3.0](https://gitlab.com/Kusken/rez-ts/compare/v1.2.0...v1.3.0) (2020-06-14)


### Features

* **result.ts:** added assert functionality for sync result ([e50b9bb](https://gitlab.com/Kusken/rez-ts/commit/e50b9bbc49a091d42d18852e78b53b07547092a7))

# [1.2.0](https://gitlab.com/Kusken/result.ts/compare/v1.1.0...v1.2.0) (2020-06-13)


### Bug Fixes

* **result.ts:** bug caused by removal of typeId ([a91eead](https://gitlab.com/Kusken/result.ts/commit/a91eeadab7369ba7ce231b28d2bc26a9ad5b0c64)), closes [#1](https://gitlab.com/Kusken/result.ts/issues/1)


### Features

* **asyncresult.ts:** added recover functionality for async result ([e5d36b8](https://gitlab.com/Kusken/result.ts/commit/e5d36b88006a315f2b28a34568b4cd3d024003f1))

# [1.1.0](https://gitlab.com/Kusken/result.ts/compare/v1.0.0...v1.1.0) (2020-06-13)


### Features

* **result.ts:** added recover functionality for sync result ([54522a6](https://gitlab.com/Kusken/result.ts/commit/54522a60d0590286064758df5104acd15a9c8eca))

# 1.0.0 (2020-06-10)


### Features

* adding basic functionality ([ebd0445](https://gitlab.com/Kusken/result.ts/commit/ebd0445e0fe78fafd0e1ff198b1afcea5bb1865f))
